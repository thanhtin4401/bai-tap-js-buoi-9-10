// <================================================>
// function: check mã đã tồn tại
function checkCodeAlreadyExist(listStaffs, value, selectorError, name) {
    var postion = findPositionStaff(listStaffs, value);
    if (postion == -1) {
        // kim tra trong mang xem co thang nao trung ko 
        document.querySelector(selectorError).innerHTML = "";
        return true;
    }
    else {
        document.querySelector(selectorError).innerHTML = name + " đã tồn tại";
        document.querySelector(selectorError).style.display = "block";
        return false;
    }
}
// <================================================>
// function: check hàm rổng
function checkNull(value, selectorError, name) {
    if (value === '' || value <= 0) {
        document.querySelector(selectorError).innerHTML = name + ' không được bỏ trống!';
        document.querySelector(selectorError).style.display = "block";
        return false;
    }
    document.querySelector(selectorError).innerHTML = '';
    return true
}
// <================================================>
// function: check ký tự 
function checkCharacter(value, selectorError, name) {
    var regexLetter = /^[A-Z a-z]+$/; //Nhập các kí tự a->z A->Z hoặc khoảng trống không bao gồm unicode
    if (regexLetter.test(value)) { //
        document.querySelector(selectorError).innerHTML = '';
        return true;
    }
    document.querySelector(selectorError).innerHTML = name + ' phải là chữ cái !';
    document.querySelector(selectorError).style.display = "block";
    return false;
}
// <================================================>
// function: check chiều dài input
function checkLength(value, selectorError, name, minLength, maxLength) {
    if (value.length < minLength || value.length > maxLength) {
        document.querySelector(selectorError).innerHTML = name + " từ " + minLength + " đến " + maxLength;
        document.querySelector(selectorError).style.display = "block";
        return false;
    }
    document.querySelector(selectorError).innerHTML = '';
    return true;
}

// <================================================>
// function: check định dạng email
function checkEmail(value, selectorError, name) {
    var regexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (regexEmail.test(value)) {
        document.querySelector(selectorError).innerHTML = "";
        return true;
    }
    document.querySelector(selectorError).innerHTML = name + ' Không đúng định dạng';
    document.querySelector(selectorError).style.display = "block";
    return false;
}

// <================================================>
// function: check số trong input
function checkNumber(value, selectorError, name) {
    var regexNumber = /^[0-9]+$/;
    if (regexNumber.test(value)) {
        document.querySelector(selectorError).innerHTML = "";
        return true;
    }
    document.querySelector(selectorError).innerHTML = name + 'tat ca phai la so';
    return false;
}

// <================================================>
// function: check số lương quy định
function checkSalary(value, selectorError, name, minSalary, maxSalary) {
    if (value >= minSalary && value <= maxSalary) {
        document.querySelector(selectorError).innerHTML = "";
        return true;
    }
    document.querySelector(selectorError).innerHTML = name + " từ " + minSalary + " đến " + maxSalary;
    document.querySelector(selectorError).style.display = "block";
}
// <================================================>
// function: check thời gian làm việc
function checkTimeWork(value, selectorError, name, timeMin, timeMax) {
    if (value >= timeMin && value <= timeMax) {
        document.querySelector(selectorError).innerHTML = "";

        return true;
    }
    document.querySelector(selectorError).style.display = "block";
    document.querySelector(selectorError).innerHTML = name + " Từ " + timeMin + " Đến " + timeMax
    return false;
}
// <================================================>
// function: check định dạng format của ngày làm
function checkDate(value, selectorError, name) {
    var convert = new Date(value);
    if (convert == "Invalid Date") {
        document.querySelector(selectorError).innerHTML = name + `định dạng mm/dd/yyyy`;
        document.querySelector(selectorError).style.display = "block";
        return false;
    }
    document.querySelector(selectorError).innerHTML = "";
    return true;
}
// <================================================>
// function: check mật khẩu
// check specialCharactor
function containsSpecialChars(str) {
    const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    return specialChars.test(str);
}
// validation passWord
// check uppercase
var isUppercase = (pass) => {
    var isfalse = false;
    for (i in pass) {
        if (pass[i] == pass[i].toUpperCase()) {
            isfalse = true;
            return isfalse;
        }
    }
    return isfalse;
}
// check specialCharactor
function containsSpecialChars(str) {
    const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    return specialChars.test(str);
}

function checkPassWord(value, selectorError, name) {
    var isStaffPassWord = /\d/.test(value);
    var isUppercasePassWord = isUppercase(value);
    var isSpecialCharactor = containsSpecialChars(value);

    if (isStaffPassWord && isUppercasePassWord && isSpecialCharactor) {
        document.querySelector(selectorError).innerHTML = "";
        return true;
    }
    else {
        document.querySelector(selectorError).innerHTML = name + " (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt), Không bỏ trống ";
        document.querySelector(selectorError).style.display = "block";
        return false;
    }
}