


// ADD
function addStaff() {
    var staff = getInfomationStaff();
    var valid = true;

    // check name
    valid &=
        checkNull(staff.account, "#tbTKNV", "Tai Khoan")
        & checkNull(staff.fullName, "#tbTen", "Họ và tên")
        & checkNull(staff.email, "#tbEmail", "Email")
        & checkNull(staff.regency, "#tbChucVu", "Chức vụ")
        & checkNull(staff.workTime, "#tbGiolam", "thời gian làm việc")
        & checkNull(staff.passWord, "#tbMatKhau", "Mật khẩu")
        & checkNull(staff.date, "#tbNgay", "Ngày làm")
        & checkNull(staff.salary, "#tbLuongCB", "Lương cơ bản");

    // check Code
    if (checkNull(staff.account, "#tbTKNV", "Tai Khoan")) {
        valid &= checkNumber(staff.account, "#tbTKNV", "Tai Khoan");
        if (checkNumber(staff.account, "#tbTKNV", "Tai Khoan")) {
            valid &= checkCodeAlreadyExist(listStaffs, staff.account, "#tbTKNV", "Tai Khoan");
            if (checkCodeAlreadyExist(listStaffs, staff.account, "#tbTKNV", "Tai Khoan")) {
                valid &= checkLength(staff.account, "#tbTKNV", "Tai Khoan", 4, 6);
            }
        }
    }

    // check name
    if (checkNull(staff.fullName, "#tbTen", "Họ và tên")) {
        valid &= checkCharacter(staff.fullName, "#tbTen", "Họ và tên");
    }

    // check email
    if (checkNull(staff.email, "#tbEmail", "Email")) {
        valid &= checkEmail(staff.email, "#tbEmail", "Email");
    }

    // check password
    if (checkNull(staff.passWord, "#tbMatKhau", "Mật khẩu")) {
        valid &= checkLength(staff.passWord, "#tbMatKhau", "Mật khẩu", 4, 10);
        if (checkLength(staff.passWord, "#tbMatKhau", "Mật khẩu", 4, 10)) {
            valid &= checkPassWord(staff.passWord, "#tbMatKhau", "Mật khẩu");
        }
    }

    // check salary
    if (checkNull(staff.salary, "#tbLuongCB", "Lương cơ bản")) {
        checkSalary(staff.salary, "#tbLuongCB", "Lương cơ bản", 1000000, 20000000);
    }
    // check time for work
    if (checkNull(staff.workTime, "#tbGiolam", "thời gian làm việc")) {
        checkTimeWork(staff.workTime, "#tbGiolam", "thời gian làm việc", 80, 200);
    }
    console.log('valid: ', valid);
    if (valid == 1) {
        console.log('valid: ', valid);


        listStaffs.push(staff);
        renderListStaffs(listStaffs);
        saveLocalStorage(listStaffs);
    }

}
// delete
function deleteStaff(account) {
    var position = findPositionStaff(listStaffs, account);
    if (position != -1) {
        listStaffs.splice(position, 1);
    }
    saveLocalStorage(listStaffs);
    renderListStaffs(listStaffs);

}

// edit
function editStaff(account) {
    var position = findPositionStaff(listStaffs, account);
    if (position == -1) {
        return;
    }
    var staff = listStaffs[position];
    getShowInformationStaff(staff);

}

// update
function update() {
    var staff = getInfomationStaff();
    var position = findPositionStaff(listStaffs, staff.account);
    listStaffs[position] = staff;
    saveLocalStorage(listStaffs);
    renderListStaffs(listStaffs);
}

// search
function searchTable(listStaffs, value) {
    var container = [];
    for (var i = 0; i < listStaffs.length; i++) {
        value = value.toLowerCase();
        var name = listStaffs[i].employeeRating().toLowerCase();
        if (name.includes(value)) {
            container.push(listStaffs[i]);
        }
    }
    return container;
}

