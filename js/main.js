
// Get Information Staff
function getInfomationStaff() {
    var account = document.getElementById("tknv").value;
    var fullName = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var passWord = document.getElementById("password").value;
    var date = document.getElementById("datepicker").value;
    var salary = document.getElementById("luongCB").value * 1;
    var regency = document.getElementById("chucvu").value;
    var workTime = document.getElementById("gioLam").value * 1;
    var staff = new staffs(account
        , fullName
        , email
        , passWord
        , date
        , salary
        , regency
        , workTime);
    return staff;
}

// tao mang lu cac thong tin cua doi tuong
var listStaffs = []



document.getElementById("btnThemNV").addEventListener("click", addStaff);



function getShowInformationStaff(staff) {
    document.getElementById("tknv").value = staff.account;
    document.getElementById("name").value = staff.fullName;
    document.getElementById("email").value = staff.email;
    document.getElementById("password").value = staff.passWord;
    document.getElementById("datepicker").value = staff.date;
    document.getElementById("luongCB").value = staff.salary;
    document.getElementById("chucvu").value = staff.regency;
    document.getElementById("gioLam").value = staff.workTime;
}


document.getElementById("btnCapNhat").addEventListener("click", update);

// find staff position in list staff
function findPositionStaff(listStaffs, account) {
    return listStaffs.findIndex(function (item) {
        return item.account == account;
    })
}


// search
var searchInput = document.getElementById("searchName");
searchInput.onkeyup = function (e) {
    // console.log();
    var value = e.target.value
    var data = searchTable(listStaffs, value);
    renderListStaffs(data);

}





// render element
function renderListStaffs(listStaffs) {
    var contentHTML = "";
    for (var index = 0; index < listStaffs.length; index++) {
        var staff = listStaffs[index];
        var contentTr = `
        <tr>
            <td>${staff.account}</td>
            <td>${staff.fullName}</td>
            <td>${staff.email}</td>
            <td>${staff.date}</td>
            <td>${staff.regency}</td>
            <td>${staff.sumSalary()}</td>
            <td>${staff.employeeRating()}</td>
            <td class="d-flex ">
                <button id="btnEditStaffInfor" data-toggle="modal" data-target="#myModal" class="btn btn-success mr-2" onClick="editStaff(${staff.account})">Edit</button>
                <button id="btnDeleteStaff" class="btnDeleteStaff btn btn-warning" onClick="deleteStaff(${staff.account})">Delete</button>
            </td>

            </tr>`;
        contentHTML += contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;

}


function saveLocalStorage(listStaffs) {
    var listStaffJSON = JSON.stringify(listStaffs);
    localStorage.setItem("listStaffsJSON", listStaffJSON);
}

var dataListStaffs = localStorage.getItem("listStaffsJSON");

if (dataListStaffs != null) {
    var arrayListStaff = JSON.parse(dataListStaffs);
    for (var index = 0; index < arrayListStaff.length; index++) {
        var item = arrayListStaff[index];

        var staff = new staffs(
            item.account,
            item.fullName,
            item.email,
            item.passWord,
            item.date,
            item.salary,
            item.regency,
            item.workTime);
        listStaffs.push(staff);


    }
    renderListStaffs(listStaffs);
}







