// tao lop nhan vien
function staffs(account, fullName, email, passWord, date, salary, regency, workTime) {
    this.account = account;
    this.fullName = fullName;
    this.email = email;
    this.passWord = passWord;
    this.date = date;
    this.salary = salary;
    this.regency = regency;
    this.workTime = workTime;
    this.sumSalary = () => {
        if (this.regency == "Sep") {
            return this.salary * 3;
        }
        else if (this.regency == "Truong Phong") {
            return this.salary * 2;
        }
        else {
            return this.salary;
        }
    }
    this.employeeRating = () => {
        if (this.workTime >= 192) {
            return "Nhân Viên Xuất Sắc";
        }
        else if (this.workTime >= 176) {
            return "Nhân Viên Giỏi";
        }
        else if (this.workTime >= 160) {
            return "Nhân Viên Khá";
        }
        else if (this.workTime < 160) {
            return "Nhân Viên Trung Bình";
        }
    }
}